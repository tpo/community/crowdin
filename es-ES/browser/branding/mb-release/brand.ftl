# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Navegador Mullvad
-brand-short-name = Navegador Mullvad
-brand-full-name = Navegador Mullvad
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Navegador Mullvad
-vendor-short-name = Mullvad
trademarkInfo = Navegador Mullvad y los logotipos del Navegador Mullvad son marcas comerciales de Mullvad VPN AB.
