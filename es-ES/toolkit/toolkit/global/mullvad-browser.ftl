# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } es un navegador web centrado en la privacidad que se ha desarrollado de forma conjunta entre <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> y el <label data-l10n-name="mullvad-about-torProjectLink">Proyecto Tor</label>. Se ha creado para minimizar el seguimiento y las huellas digitales.
mullvad-about-readMore = Más información
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Ayuda y comentarios: { $emailAddress }
mullvad-about-telemetryLink = Telemetría

## Mullvad browser home page.

about-mullvad-browser-developed-by = Desarrollado de forma conjunta entre el <a data-l10n-name="tor-project-link">Proyecto Tor</a> y <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Mejora tu privacidad usando el navegador <a data-l10n-name="with-vpn-link">con Mullvad VPN</a>.
about-mullvad-browser-learn-more = ¿Tienes curiosidad por conocer mejor el navegador? <a data-l10n-name="learn-more-link">Entra en la madriguera del topo</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Página de inicio del { -brand-product-name }

## about:rights page.

rights-mullvad-intro = El { -brand-short-name } es una aplicación libre y de código abierto.
rights-mullvad-you-should-know = Hay algunas cosas que debes saber:
rights-mullvad-trademarks =
    No se te conceden derechos de marca comercial ni licencias para las marcas comerciales
    de { -brand-short-name } o cualquier parte, incluyendo sin limitaciones el nombre o logotipo de
    { -brand-short-name }.

## about:telemetry page.

telemetry-title = Información sobre telemetría
telemetry-description = La telemetría está desactivada en { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
