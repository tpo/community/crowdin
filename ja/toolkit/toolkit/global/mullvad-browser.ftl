# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } は <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> と <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> が共同開発したプライバシー重視の Web ブラウザーです。トラッキングやフィンガープリンティングを最小限に抑えるために開発されました。
mullvad-about-readMore = 詳細情報
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = テレメトリ

## Mullvad browser home page.

about-mullvad-browser-developed-by = <a data-l10n-name="tor-project-link">Tor Project</a> と <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> による共同開発です。
about-mullvad-browser-use-vpn = <a data-l10n-name="with-vpn-link">Mullvad VPN</a> を介してブラウザーを利用することでプライバシーを強化できます。
about-mullvad-browser-learn-more = このブラウザーについて興味をお持ちですか？<a data-l10n-name="learn-more-link">一緒にこのモグラの穴を探検してみましょう</a>。
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } ホーム

## about:rights page.

rights-mullvad-intro = { -brand-short-name } は無料のオープンソースソフトウェアです。
rights-mullvad-you-should-know = 以下の点をご了承ください。
rights-mullvad-trademarks = ユーザーには { -brand-short-name } またはすべての当事者の登録商標 ({ -brand-short-name } の名称またはロゴマークを含みますが、これらに限定されません) の商標権やライセンスは付与されません。

## about:telemetry page.

telemetry-title = テレメトリ情報
telemetry-description = { -brand-short-name } ではテレメトリは無効化されています。

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
