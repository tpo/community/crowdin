# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } est un navigateur Web axé sur la protection de la vie privée, développé en collaboration entre <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> et le <label data-l10n-name="mullvad-about-torProjectLink">Projet Tor</label>. Il est conçu pour minimiser le suivi et le profilage.
mullvad-about-readMore = En savoir plus
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Télémétrie

## Mullvad browser home page.

about-mullvad-browser-developed-by = Développé en collaboration entre le <a data-l10n-name="tor-project-link">Projet Tor</a> et <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Renforcez votre confidentialité en utilisant le navigateur <a data-l10n-name="with-vpn-link">avec Mullvad VPN</a>.
about-mullvad-browser-learn-more = Curieux d'en savoir plus sur le navigateur ? <a data-l10n-name="learn-more-link">Plongez dans les profondeurs</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Accueil de { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } est un logiciel gratuit et open source.
rights-mullvad-you-should-know = Voici quelques informations à connaître :
rights-mullvad-trademarks =
    Vous ne bénéficiez d'aucun droit sur les marques déposées ni d'aucune licence sur les marques déposées de
    { -brand-short-name } ou de toute autre partie, y compris, mais sans s'y limiter,
    le nom ou le logo de { -brand-short-name }.

## about:telemetry page.

telemetry-title = Informations sur la télémétrie
telemetry-description = La télémétrie est désactivée dans { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
