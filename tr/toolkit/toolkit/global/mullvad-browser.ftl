# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }, <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> ve <label data-l10n-name="mullvad-about-torProjectLink">Tor Projesi</label> iş birliğiyle geliştirilen gizlilik odaklı bir web tarayıcısıdır. İzlenmeyi ve parmak izi almayı en aza indirecek şekilde tasarlanmıştır.
mullvad-about-readMore = Devamını oku
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetri

## Mullvad browser home page.

about-mullvad-browser-developed-by = <a data-l10n-name="tor-project-link">Tor Projesi</a> ve <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> arasındaki iş birliğiyle geliştirilmiştir
about-mullvad-browser-use-vpn = <a data-l10n-name="with-vpn-link">Mullvad VPN</a> bulunan tarayıcıyı kullanarak daha fazla gizlilik elde edin.
about-mullvad-browser-learn-more = Tarayıcı hakkında daha fazla bilgi edinmek ister misiniz? <a data-l10n-name="learn-more-link">Daha fazla bilgi edinin</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } Ana Sayfa

## about:rights page.

rights-mullvad-intro = { -brand-short-name } ücretsiz ve açık kaynaklı bir yazılımdır.
rights-mullvad-you-should-know = Bilmeniz gerekenler:
rights-mullvad-trademarks =
    { -brand-short-name } veya logosu dahil, ancak bunlarla sınırlı olmamak üzere,
    size { -brand-short-name } veya herhangi bir tarafın ticari markaları üzerinde
    hiçbir ticari marka hakkı veya lisansı verilmez.

## about:telemetry page.

telemetry-title = Telemetri Bilgileri
telemetry-description = { -brand-short-name } için telemetri devre dışı bırakıldı.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
