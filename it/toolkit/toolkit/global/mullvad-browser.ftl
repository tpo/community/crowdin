# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = Il { -brand-short-name } è un browser web incentrato sulla privacy sviluppato in collaborazione tra <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> e il <label data-l10n-name ="mullvad-about-torProjectLink">Progetto Tor</label>. È progettato per ridurre al minimo le tracce e le impronte.
mullvad-about-readMore = Scopri di più
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetria

## Mullvad browser home page.

about-mullvad-browser-developed-by = Sviluppato in collaborazione tra il <a data-l10n-name="tor-project-link">Progetto Tor</a> e <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Ottieni maggiore privacy utilizzando il browser <a data-l10n-name="with-vpn-link">con Mullvad VPN</a>.
about-mullvad-browser-learn-more = Vuoi scoprire di più sul browser? <a data-l10n-name="learn-more-link">Immergiti nella tana della talpa</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Pagina principale del { -brand-product-name }

## about:rights page.

rights-mullvad-intro = Il { -brand-short-name } è un software gratuito e open source.
rights-mullvad-you-should-know = Ci sono alcune cose che dovresti sapere:
rights-mullvad-trademarks = Non ti viene concesso alcun diritto di marchio o licenza sui marchi del { -brand-short-name } o qualsiasi parte, incluso senza limitazioni il nome o il logo del { -brand-short-name }.

## about:telemetry page.

telemetry-title = Informazioni sulla telemetria
telemetry-description = La telemetria è disabilitata nel { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
