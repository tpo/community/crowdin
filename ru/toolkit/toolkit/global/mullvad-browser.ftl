# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } – веб-браузер, ориентированный на конфиденциальность, разработанный в сотрудничестве между <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> и <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Он создан для минимизации отслеживания и цифрового отпечатка.
mullvad-about-readMore = Подробнее
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Телеметрия

## Mullvad browser home page.

about-mullvad-browser-developed-by = Разработан в сотрудничестве между <a data-l10n-name="tor-project-link">Tor Project</a> и <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Усильте свою конфиденциальность в сети, используя браузер <a data-l10n-name="with-vpn-link">с Mullvad VPN</a>.
about-mullvad-browser-learn-more = Хотите узнать больше о браузере? <a data-l10n-name="learn-more-link">Подробнее — здесь</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Начальная страница ПО «{ -brand-product-name }»

## about:rights page.

rights-mullvad-intro = { -brand-short-name } — это бесплатное программное обеспечение с открытым исходным кодом.
rights-mullvad-you-should-know = Вам следует принять к сведению следующее:
rights-mullvad-trademarks =
    Вам не предоставляется никаких прав или лицензий на товарные знаки
    ПО «{ -brand-short-name }» и любой иной стороны, включая, среди прочего, название и логотип «{ -brand-short-name }».

## about:telemetry page.

telemetry-title = Информация о телеметрии
telemetry-description = Телеметрия отключена в ПО «{ -brand-short-name }».

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
