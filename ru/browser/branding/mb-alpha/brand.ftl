# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Браузер Mullvad
-brand-short-name = Mullvad Browser Alpha
-brand-full-name = Mullvad Browser Alpha
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Браузер Mullvad
-vendor-short-name = Mullvad
trademarkInfo = Название и логотип браузера Mullvad являются зарегистрированными товарными знаками компании Mullvad VPN AB.
