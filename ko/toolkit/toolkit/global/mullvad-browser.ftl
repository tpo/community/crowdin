# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }는 <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label>과 <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>가 협업하여 개발한, 개인 정보 보호에 중점을 둔 웹 브라우저입니다. 추적과 디지털 지문을 최소화할 수 있도록 제작되었죠.
mullvad-about-readMore = 더 알아보기
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = 원격 측정

## Mullvad browser home page.

about-mullvad-browser-developed-by = <a data-l10n-name="tor-project-link">Tor Project</a>와 <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>의 협업으로 개발되었습니다.
about-mullvad-browser-use-vpn = <a data-l10n-name="with-vpn-link">Mullvad VPN</a>과 함께 브라우저를 사용해 개인 정보 보호를 강화하세요.
about-mullvad-browser-learn-more = 이 브라우저에 대해 자세히 알아보고 싶으신가요? <a data-l10n-name="learn-more-link">여기에서 확인하실 수 있습니다</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } 홈

## about:rights page.

rights-mullvad-intro = { -brand-short-name }는 무료 및 오픈 소스 소프트웨어입니다.
rights-mullvad-you-should-know = 몇 가지 유의 사항을 알려드립니다.
rights-mullvad-trademarks = 귀하에게 { -brand-short-name } 또는 기타 당사자의 상표권 또는 라이선스가 허여된 것은 아닙니다. 여기에는 { -brand-short-name } 이름 또는 로고가 포함되며 이에 국한되지 않습니다.

## about:telemetry page.

telemetry-title = 원격 측정 정보
telemetry-description = { -brand-short-name }에서 원격 측정은 비활성화되어 있습니다.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
