# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } é um navegador da Web com foco na privacidade que foi desenvolvido em colaboração entre a <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> e o <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Ele foi criado para minimizar o rastreamento e a impressão digital.
mullvad-about-readMore = Ler mais
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetria

## Mullvad browser home page.

about-mullvad-browser-developed-by = Desenvolvido em colaboração entre o <a data-l10n-name="tor-project-link">Tor Project</a> e a <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Tenha mais privacidade usando o navegador <a data-l10n-name="with-vpn-link">com a Mullvad VPN</a>.
about-mullvad-browser-learn-more = Curioso para saber mais sobre o navegador? <a data-l10n-name="learn-more-link">Mergulhe fundo</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Página inicial do { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } é um software livre e de código aberto.
rights-mullvad-you-should-know = Algumas coisas que você precisa saber:
rights-mullvad-trademarks =
    Você não recebe nenhum direito de marca registrada nem licença para as marcas registradas
    { -brand-short-name } ou de qualquer parte, incluindo, sem limitação, o
    nome ou logotipo { -brand-short-name }.

## about:telemetry page.

telemetry-title = Informações de telemetria
telemetry-description = A telemetria está desativada no { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
