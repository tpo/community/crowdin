# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } ist ein auf Datenschutz ausgerichteter Webbrowser, entwickelt in Zusammenarbeit zwischen <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> und dem <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Er wurde entworfen, um Tracking und digitale Fingerabdrücke zu minimieren.
mullvad-about-readMore = Mehr erfahren
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Hilfe & Feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetrie

## Mullvad browser home page.

about-mullvad-browser-developed-by = Entwickelt in Zusammenarbeit zwischen dem <a data-l10n-name="tor-project-link">Tor Project</a> und <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Genießen Sie mehr Datenschutz, indem Sie den Browser <a data-l10n-name="with-vpn-link">mit Mullvad VPN</a> verwenden.
about-mullvad-browser-learn-more = Sind wollen mehr über den Browser erfahren? <a data-l10n-name="learn-more-link">Dann folgen Sie diesem Link</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } Start

## about:rights page.

rights-mullvad-intro = { -brand-short-name } ist eine kostenlose Open-Source-Software.
rights-mullvad-you-should-know = Hier paar Dinge, die Sie wissen sollten:
rights-mullvad-trademarks =
    Es werden Ihnen keine Markenrechte oder Lizenzen an den Marken von
    { -brand-short-name } oder einer anderen Partei gewährt, einschließlich, aber
    nicht beschränkt auf Name oder Logo von { -brand-short-name }.

## about:telemetry page.

telemetry-title = Telemetrie-Informationen
telemetry-description = Telemetrie ist in { -brand-short-name } deaktiviert.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
