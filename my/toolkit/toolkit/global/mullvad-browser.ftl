# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } သည် <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> နှင့် <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> တို့ ပူးပေါင်းဆောင်ရွက်၍ ဖန်တီးထားသည့် ကိုယ်ပိုင်အချက်အလက် လုံခြုံရေးကို အလေးထားသော ဝက်ဘ်ဘရောက်ဇာတစ်ခု ဖြစ်ပါသည်။ ခြေရာခံနှင့် လက်ဗွေရာမှတ်သားခြင်းကို အနည်းဆုံးဖြစ်အောင် လျှော့ချရန် ၎င်းကို ထုတ်လုပ်ထားပါသည်။
mullvad-about-readMore = ပိုမိုဖတ်ရှုရန်
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = တယ်လီမက်ထရီ

## Mullvad browser home page.

about-mullvad-browser-developed-by = <a data-l10n-name="tor-project-link">Tor Project</a> နှင့် <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> အကြား ပူးပေါင်းဆောင်ရွက်၍ ဖန်တီးထားသည်
about-mullvad-browser-use-vpn = ဘရောက်ဇာကို <a data-l10n-name="with-vpn-link">Mullvad VPN</a> နှင့်အတူ သုံးစွဲခြင်းအားဖြင့် ကိုယ်ရေးအချက်အလက် လုံခြုံမှု ပိုမိုရရှိစေပါသည်။
about-mullvad-browser-learn-more = ဘရောက်ဇာအကြောင်း ပိုမိုလေ့လာရန် စိတ်ဝင်စားပါသလား။ <a data-l10n-name="learn-more-link">အတွင်းကျကျ လေ့လာလိုက်ပါ</a>။
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } ပင်မ

## about:rights page.

rights-mullvad-intro = { -brand-short-name } သည် အခမဲ့ဖြစ်ပြီး Open source ဆော့ဖ်ဝဲ ဖြစ်ပါသည်။
rights-mullvad-you-should-know = ဤသည်တို့မှာ သင်သိသင့်သော အရာအနည်းငယ် ဖြစ်ပါသည်-
rights-mullvad-trademarks = { -brand-short-name } နာမည် သို့မဟုတ် လိုဂို အကန့်အသတ်မရှိ ပါဝင်လျက် { -brand-short-name } ၏ ကုန်အမှတ်တံဆိပ်များ သို့မဟုတ် အဖွဲ့အစည်းတစ်ခုခုအတွက် မည်သည့် ကုန်အမှတ်တံဆိပ် အခွင့်အရေးများ သို့မဟုတ် လိုင်စင်များကိုမျှ သင့်အား မပေးအပ်ထားပါ။

## about:telemetry page.

telemetry-title = တယ်လီမက်ထရီ အချက်အလက်
telemetry-description = { -brand-short-name } တွင် တယ်လီမက်ထရီကို ပိတ်ထားပါသည်။

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
