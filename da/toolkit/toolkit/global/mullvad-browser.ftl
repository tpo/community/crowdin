# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } er en webbrowser med fokus på privatliv, som er udviklet i et samarbejde mellem <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> og <label data-l10n-name="mullvad-about-torProjectLink">Tor-projektet</label>. Den er fremstillet til at minimere sporing og registrering af fingeraftryk.
mullvad-about-readMore = Læs mere
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Hjælp og feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetri

## Mullvad browser home page.

about-mullvad-browser-developed-by = Udviklet i et samarbejde mellem <a data-l10n-name="tor-project-link">Tor-projektet</a> og <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Få bedre beskyttelse af privatlivet ved at bruge browseren <a data-l10n-name="with-vpn-link">med Mullvad VPN</a>.
about-mullvad-browser-learn-more = Nysgerrig efter at lære mere om browseren? <a data-l10n-name="learn-more-link">Dyk ned i muldvarpehullet</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Startside for { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } er gratis og open source-software.
rights-mullvad-you-should-know = Der er et par ting, du bør vide:
rights-mullvad-trademarks =
    Du tildeles ikke nogen varemærkerettigheder eller licenser til 
    varemærkerne tilhørende 
    { -brand-short-name } eller nogen anden part, herunder, men ikke begrænset til navnet eller logoet fra { -brand-short-name }.

## about:telemetry page.

telemetry-title = Telemetri-information
telemetry-description = Telemetri er deaktiveret i { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
