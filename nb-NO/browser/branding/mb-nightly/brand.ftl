# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Mullvad-nettleser
-brand-short-name = Mullvad Browser Nightly
-brand-full-name = Mullvad Browser Nightly
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Mullvad-nettleser
-vendor-short-name = Mullvad
trademarkInfo = Mullvad-nettleser og Mullvad-nettleser-logoer er varemerker som tilhører Mullvad VPN AB.
