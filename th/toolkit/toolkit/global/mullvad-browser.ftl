# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } เป็นเว็บเบราว์เซอร์ที่ให้ความสำคัญกับความเป็นส่วนตัว ซึ่งเป็นการพัฒนาร่วมกันระหว่าง <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> และ <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> โดยได้รับการพัฒนาขึ้นเพื่อลดการติดตาม และปกปิดร่องรอยการใช้งาน
mullvad-about-readMore = อ่านเพิ่มเติม
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = โทรมาตร

## Mullvad browser home page.

about-mullvad-browser-developed-by = พัฒนาร่วมกันระหว่าง <a data-l10n-name="tor-project-link">Tor Project</a> และ <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = รับความเป็นส่วนตัวมากขึ้น โดยใช้เบราว์เซอร์<a data-l10n-name="with-vpn-link">ร่วมกับ Mullvad VPN</a>
about-mullvad-browser-learn-more = อยากรู้ข้อมูลเพิ่มเติมเกี่ยวกับเบราว์เซอร์ไหม <a data-l10n-name="learn-more-link">เข้ามาเรียนรู้เลยสิ</a>
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = หน้าหลัก { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } ไม่มีค่าใช้จ่าย และเป็นซอฟตแวร์โอเพนซอร์ซ
rights-mullvad-you-should-know = มีสองสามสิ่งที่คุณควรทราบ:
rights-mullvad-trademarks =
    คุณไม่ได้รับสิทธิ์ในเครื่องหมายการค้าหรือใบอนุญาต สำหรับเครื่องหมายการค้าของ
    { -brand-short-name } หรือฝ่ายใดฝ่ายหนึ่ง รวมถึงแต่ไม่จำกัดเพียง
    ชื่อหรือโลโก้ของ { -brand-short-name }

## about:telemetry page.

telemetry-title = ข้อมูลโทรมาตร
telemetry-description = โทรมาตรถูกปิดใช้งานใน { -brand-short-name }

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
