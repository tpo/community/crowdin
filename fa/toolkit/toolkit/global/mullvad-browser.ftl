# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } یک مرورگر وبِ متمرکز بر حریم خصوصی است که با همکاری <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> و <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> توسعه پیدا کرده است. این مرورگر وب برای به حداقل رساندن ردیابی و عدم افشای اطلاعات تولید شده است.
mullvad-about-readMore = بیشتر بخوانید
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = راهنما و بازخورد: { $emailAddress }
mullvad-about-telemetryLink = تله‌متری

## Mullvad browser home page.

about-mullvad-browser-developed-by = از همکاری میان <a data-l10n-name="tor-project-link">Tor Project</a> و <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> ایجاد شده است
about-mullvad-browser-use-vpn = با به‌کارگیری مرورگر <a data-l10n-name="with-vpn-link">بواسطۀ Mullvad VPN</a> از حریم خصوصی بیشتری برخوردار شوید.
about-mullvad-browser-learn-more = کنجکاو هستید در خصوص این مرورگر بیشتر بدانید؟ <a data-l10n-name="learn-more-link">اطلاعات بیشتری دربارۀ این سیستم پیچیده بدست بیاورید</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } خانه

## about:rights page.

rights-mullvad-intro = { -brand-short-name } یک نرم‌افزار رایگان و منبع‌ باز است.
rights-mullvad-you-should-know = چند نکته وجود دارد که باید بدانید:
rights-mullvad-trademarks = برای علائم تجاری { -brand-short-name } یا هیچ یک از طرفین، هیچ گونه حق علامت تجاری یا مجوزی به شما اعطا نمی شود، از جمله بدون محدودیت نام یا لوگوی { -brand-short-name }.

## about:telemetry page.

telemetry-title = اطلاعات تله‌متری
telemetry-description = تله‌متری در { -brand-short-name } غیرفعال است.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
