# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }  är en sekretessfokuserad webbläsare som utvecklats i samarbete med <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> och <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Den har skapats för att minimera spårning och fingeravtryck.
mullvad-about-readMore = Läs mer
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetri

## Mullvad browser home page.

about-mullvad-browser-developed-by = Utvecklat i samarbete med <a data-l10n-name="tor-project-link">Tor Project</a> och <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Få mer sekretess genom att använda webbläsaren <a data-l10n-name="with-vpn-link">med Mullvad VPN</a>.
about-mullvad-browser-learn-more = Vill du veta mer om webbläsaren? <a data-l10n-name="learn-more-link">Dyk ned i mullvadshålet</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Startsida för { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } är gratis programvara med öppen källkod.
rights-mullvad-you-should-know = Det här bör du veta:
rights-mullvad-trademarks = Du får inga varumärkesrättigheter eller licenser för varumärken som tillhör { -brand-short-name } eller någon part, inklusive och utan begränsning till namn eller logotyp för { -brand-short-name }.

## about:telemetry page.

telemetry-title = Information om telemetri
telemetry-description = Telemetri är inaktiverat för { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
