# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } to zorientowana na prywatność przeglądarka internetowa opracowana we współpracy między <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> a <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. Jej celem jest minimalizowanie śledzenia i zbierania prywatnych danych w celach identyfikacji.
mullvad-about-readMore = Dowiedz się więcej
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetria

## Mullvad browser home page.

about-mullvad-browser-developed-by = Stworzono we współpracy między <a data-l10n-name="tor-project-link">Tor Project</a> a <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Zyskaj większą prywatność, korzystając z przeglądarki <a data-l10n-name="with-vpn-link">z Mullvad VPN</a>.
about-mullvad-browser-learn-more = Chcesz dowiedzieć się więcej o przeglądarce? <a data-l10n-name="learn-more-link">Weź ją pod lupę</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = Strona główna { -brand-product-name }

## about:rights page.

rights-mullvad-intro = { -brand-short-name } jest darmowym i otwartym oprogramowaniem.
rights-mullvad-you-should-know = Jest kilka rzeczy, które należy wiedzieć:
rights-mullvad-trademarks =
    Użytkownikowi nie przyznaje się żadnych praw do znaków towarowych ani licencji na znaki towarowe firmy
    { -brand-short-name } lub jakiejkolwiek strony, w tym bez wyjątku do nazwy i logotypu
    { -brand-short-name }.

## about:telemetry page.

telemetry-title = Informacje dotyczące telemetrii
telemetry-description = W { -brand-short-name } telemetria jest wyłączona.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
