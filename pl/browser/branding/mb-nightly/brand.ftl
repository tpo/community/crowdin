# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Przeglądarka Mullvad
-brand-short-name = Mullvad Browser Nightly
-brand-full-name = Mullvad Browser Nightly
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Przeglądarka Mullvad
-vendor-short-name = Mullvad
trademarkInfo = Nazwa Przeglądarka Mullvad i logotypy Przeglądarki Mullvad są znakami towarowymi firmy Mullvad VPN AB.
