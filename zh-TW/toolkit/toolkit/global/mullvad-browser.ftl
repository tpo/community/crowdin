# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name } 是一款注重隱私的網頁瀏覽器，由 <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> 和 <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> 合作開發，能有效減少追蹤和指紋識別。
mullvad-about-readMore = 繼續閱讀
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = 遙測

## Mullvad browser home page.

about-mullvad-browser-developed-by = 由 <a data-l10n-name="tor-project-link">Tor Project</a> 和 <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> 合作開發
about-mullvad-browser-use-vpn = 使用 <a data-l10n-name="with-vpn-link">Mullvad VPN</a> 瀏覽器，享有更多隱私。
about-mullvad-browser-learn-more = 想深入了解瀏覽器嗎？快來<a data-l10n-name="learn-more-link">一探究竟</a>吧。
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } 首頁

## about:rights page.

rights-mullvad-intro = { -brand-short-name } 是開放原始碼的免費軟體。
rights-mullvad-you-should-know = 注意事項：
rights-mullvad-trademarks = 您並未獲得任何對 { -brand-short-name } 或任何一方的商標權利與授權，包括但不限於 { -brand-short-name } 的名稱或標誌。

## about:telemetry page.

telemetry-title = 遙測資訊
telemetry-description = 遙測已在 { -brand-short-name } 中停用。

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
