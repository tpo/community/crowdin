# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-brand-shorter-name = Mullvad 瀏覽器
-brand-short-name = Mullvad Browser Alpha
-brand-full-name = Mullvad Browser Alpha
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Mullvad 瀏覽器
-vendor-short-name = Mullvad
trademarkInfo = Mullvad 瀏覽器與 Mullvad 瀏覽器標誌均為 Mullvad VPN AB 的商標。
