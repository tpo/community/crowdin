# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = إن { -brand-short-name } متصفح للويب يركز على الخصوصية، تم تطويره بالتعاون بين <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> و<label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. إنه مُصمَّم لتقييد التعقب وتقليل الأثر المتروك بعد تصفح الويب.
mullvad-about-readMore = قراءة المزيد
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = للمساعدة والملاحظات: { $emailAddress }
mullvad-about-telemetryLink = القياس عن بعد

## Mullvad browser home page.

about-mullvad-browser-developed-by = تم تطويره بالتعاون بين <a data-l10n-name="tor-project-link">Tor Project</a> و<a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = تمتع بخصوصية أكبر باستخدام المتصفح مع <a data-l10n-name="with-vpn-link">Mullvad VPN</a>.
about-mullvad-browser-learn-more = هل تريد معرفة المزيد عن المتصفح؟ <a data-l10n-name="learn-more-link">إذن، ابدأ من هنا للتعمق في الأمر</a>.
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = صفحة { -brand-product-name } الرئيسية

## about:rights page.

rights-mullvad-intro = إن { -brand-short-name } برنامج مجاني مفتوح المصدر.
rights-mullvad-you-should-know = عليك معرفة بعض الأمور:
rights-mullvad-trademarks =
    نحن لا نمنحك أي حقوق أو تراخيص للعلامات التجارية فيما يتعلق بـ { -brand-short-name }
    أو أي جهة ذات صلة، بما في ذلك، على سبيل المثال وليس الحصر،
    اسم "{ -brand-short-name }" أو شعاره.

## about:telemetry page.

telemetry-title = معلومات القياس عن بعد
telemetry-description = القياس عن بعد لا يعمل في { -brand-short-name }.

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
