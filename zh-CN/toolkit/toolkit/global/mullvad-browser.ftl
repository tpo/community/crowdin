# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }是一款注重隐私保护的网络浏览器，由 <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> 与 <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label> 联合开发。它可以最大程度地减少跟踪和指纹收集。
mullvad-about-readMore = 阅读更多内容
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = 遥测

## Mullvad browser home page.

about-mullvad-browser-developed-by = 由 <a data-l10n-name="tor-project-link">Tor Project</a> 与 <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a> 联合开发
about-mullvad-browser-use-vpn = 结合使用浏览器与 <a data-l10n-name="with-vpn-link">Mullvad VPN</a>，获得更多隐私。
about-mullvad-browser-learn-more = 想要详细了解浏览器？<a data-l10n-name="learn-more-link">一探究竟</a>。
# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message = { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad = 
    .label = { -brand-product-name } 首页

## about:rights page.

rights-mullvad-intro = { -brand-short-name }是一款免费且开源的软件。
rights-mullvad-you-should-know = 注意事项：
rights-mullvad-trademarks = 您未被授予 { -brand-short-name }或任何其他方的商标的任何商标权或许可，包括但不限于 { -brand-short-name }的名称或徽标。

## about:telemetry page.

telemetry-title = 遥测信息
telemetry-description = 遥测在 { -brand-short-name }中禁用。

## Will be unused in Mullvad Browser 14.5!
## Mullvad browser home page.

about-mullvad-browser-heading = { -brand-product-name }
